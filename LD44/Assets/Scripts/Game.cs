﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Game : MonoBehaviour
{
    public Transform playerPrefab;
    public CameraControl cameraControl;
    public List<string> levels = new List<string>();
    Level level;
        

    // Start is called before the first frame update
    void Start()
    {
        level = GetComponent<Level>();
        level.LoadLevel(levels[0]);

        Vector3 spawnPoint = level.GetWorldSpacePositionOfTile(level.playerSpawn.x, level.playerSpawn.y);
        spawnPoint.y += 0.2f;

        cameraControl.target = GameObject.Instantiate(playerPrefab, spawnPoint, Quaternion.identity);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            Screenshot();

        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Quit();

        }

    }




    static public void Quit()
    {


#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#elif (UNITY_WEBGL)

#else
        Application.Quit();
#endif
    }

    static public void Screenshot()
    {
        string dir = Application.persistentDataPath + "/Screenshots";
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        string timestamp = string.Format("{0:yyyyMMddHHmmss}", System.DateTime.Now);

        string filename = dir + "/img" + timestamp + ".png";

        ScreenCapture.CaptureScreenshot(filename);
        Debug.Log(filename);
        //TODO: play camera shutter noise
    }

    static public void EditorPause()
    {

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPaused = true;

#endif
    }
}

