﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor
{
    SerializedProperty width;
    SerializedProperty height;
    SerializedProperty playerSpawn;
    
    TileEditorWindow window = null;

    Level.Tile currentSelectedTile = null;
    Vector2Int currentSelectedTileCoordinates = Vector2Int.zero;

    void OnEnable()
    {
        // Setup the SerializedProperties.
        Level level = (Level)target;
        width = serializedObject.FindProperty("width");
        height = serializedObject.FindProperty("height");
        playerSpawn = serializedObject.FindProperty("playerSpawn");
        Tools.hidden = true;


    }


    private void OnDisable()
    {
        Tools.hidden = false;
    }

    override public void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();
        //

        DrawDefaultInspector();

        Level level = (Level)target;



        //Do inspector stuff 
        if (GUILayout.Button("New"))
        {

            level.InitialiseMap();

        }

        if (level.map == null) GUI.enabled = false;
        if (GUILayout.Button("Save"))
        {

            if (level.filename == null)
            {
                string timestamp = string.Format("{0:MMddHHmmss}", System.DateTime.Now);
                level.filename = "Level";
                level.filename += timestamp;

            }

            string filename = level.filename;

            level.SaveLevel(filename);
            
        }
        GUI.enabled = true;

        if (GUILayout.Button("Load"))
        {
            if (level.filename != null)
            {
                level.LoadLevel(level.filename);

            }
        }

            serializedObject.ApplyModifiedProperties();
    }

    internal void ClearSelectedTile()
    {
        currentSelectedTile = null;
    }

    private void OnSceneGUI()
    {
        
        Level level = (Level)target;
        if (!level.initialised) return;
        serializedObject.UpdateIfRequiredOrScript();

        float x = level.playerSpawn.x * level.gridSize.x + level.gridSize.x / 2;
        float y = level.playerSpawn.y * level.gridSize.y + level.gridSize.y / 2;
        Vector3 spawnPoint = new Vector3(x, y + level.gridSize.y/2, level.gridSize.z / 2);
        Vector3 size = new Vector3(level.gridSize.x / 2, level.gridSize.y*2, level.gridSize.z /2);
        Handles.color = Color.green;

        Handles.DrawWireCube(spawnPoint, size);
        
        if(currentSelectedTile != null)
        {
            x = currentSelectedTileCoordinates.x * level.gridSize.x + level.gridSize.x / 2;
            y = currentSelectedTileCoordinates.y * level.gridSize.y + level.gridSize.y / 2;
            Vector3 position = new Vector3(x, y, level.gridSize.z/2);
            size = new Vector3(level.gridSize.x, level.gridSize.y, level.gridSize.z);
            Handles.color = Color.yellow;
            
            Handles.DrawWireCube(position, size);
        }



        //TODO: really need to establish a border region that can't be edited.
        Plane plane = new Plane(Vector3.zero, Vector3.up, Vector3.right);
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        float distance;
        if(plane.Raycast(ray, out distance))
        {
            Vector3 point = ray.GetPoint(distance);

            Level.Tile tile = null;
            Vector2Int tileCoordinates = level.GetGridCoordinatesAtPoint(point.x,point.y);

            if (level.IsWithinBoundary(tileCoordinates))
            {
                tile = level.GetTileAtGridCoordinates(tileCoordinates); 
            }

            if (tile != null)
            {
                x = tileCoordinates.x * level.gridSize.x + level.gridSize.x/2;
                y = tileCoordinates.y * level.gridSize.y + level.gridSize.y / 2;
                Vector3 position = new Vector3(x, y, 0);
                Handles.color = Color.white;
                bool result = Handles.Button(position, Quaternion.identity, level.gridSize.x/2, level.gridSize.x/2, Handles.RectangleHandleCap);
                if (result)
                {
                    currentSelectedTile = tile;
                    currentSelectedTileCoordinates = tileCoordinates;

                    
                    if (tile.value !=0) tile.value = 0;
                    

                    level.Display();

                    //show window with options
                    window = (TileEditorWindow)EditorWindow.GetWindow(typeof(TileEditorWindow),true,"Edit Tile");
                    window.SetTarget(this, level, tile, tileCoordinates);
                    window.Show();
                }
             
            }
            else
            {
                if(currentSelectedTile == null && window != null)
                {
                    window.Close();
                }
            }
        }
        //do Scene UI stuff
        
        serializedObject.ApplyModifiedProperties();
        
    }


}
