﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TileEditorWindow : EditorWindow
{
    LevelEditor editor;
    Level level;
    Level.Tile tile;
    Vector2Int coordinates;
    public void SetTarget(LevelEditor _editor, Level _level, Level.Tile _tile, Vector2Int _coordinates)
    {
        editor = _editor;
        level = _level;
        tile = _tile;
        coordinates = _coordinates;
    }

    void OnGUI()
    {
        if (editor == null || level == null || tile == null || coordinates == null) return;

        string msg = "Current Tile: ";
        msg += coordinates.ToString();
        GUILayout.Label(msg, EditorStyles.boldLabel);

        if (GUILayout.Button("Set Player Spawn"))
        {
            if (tile.value == 0 && level.map[coordinates.x,coordinates.y + 1].value == 0)
            {
                level.playerSpawn = coordinates;
            }
        }

        if (GUILayout.Button("Set Tile to Solid"))
        {
            Vector2Int down = new Vector2Int( coordinates.x,coordinates.y-1);
            
            if (tile.value == 0 && coordinates != level.playerSpawn && down != level.playerSpawn)
            {
                tile.value = 1;
                level.Display();//redraw the mesh
            }
        }

    }

    void OnDestroy()
    {
        if(editor != null)
        {
            editor.ClearSelectedTile();
        }
    }
}
