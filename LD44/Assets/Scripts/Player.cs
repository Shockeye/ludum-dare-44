﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{


    public float speed = 5;
    public float jump = 5;
    public float acceleration = 0.1f;
    Vector3 velocity = Vector3.zero;
    int ground;
    int walls;
   
    // Start is called before the first frame update
    void Start()
    {
        ground = 1 << LayerMask.NameToLayer("Ground");
        walls = 1  << LayerMask.NameToLayer("Walls");

    }

    // Update is called once per frame
    void Update()
    {
        bool onGround = IsOnGround();

        if (onGround) Debug.Log("onground");
        else Debug.Log("in air");
        //Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
        float move = Input.GetAxisRaw("Horizontal");
        
        velocity.x = Mathf.MoveTowards(velocity.x, speed * move, acceleration * Time.deltaTime);

        velocity.y += Physics.gravity.y * Time.deltaTime;
        
        if (onGround && velocity.y < 0)velocity.y = 0f;
                
        if (Input.GetButtonDown("Jump") && onGround)velocity.y += jump; 

        Move(velocity * Time.deltaTime);


    }

    private void Move(Vector3 move)
    {
        Debug.DrawLine(transform.position, transform.position + move * 5f);
        if (!IsColliding(transform.position + move)) transform.Translate(move);
    }

    bool IsColliding(Vector3 position)
    {
        float distance = 0.25f;
        return Physics.CheckSphere(position, distance,walls, QueryTriggerInteraction.Ignore);
    }

    bool IsOnGround()
    {        
        float distance = 0.5f;
        
        return Physics.CheckSphere(transform.position, distance, ground, QueryTriggerInteraction.Ignore);
    }
}
