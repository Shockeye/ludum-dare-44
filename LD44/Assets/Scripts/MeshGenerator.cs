﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator 
{
    public class Vertex
    {
        public Vector3 position;
        public int index = -1;

        public Vertex(Vector3 pos)
        {
            position = pos;
        }
    }

    public class Square
    {
        public bool init = false;
        public Vertex[] vertices = new Vertex[4]; //topLeft, topRight, bottomRight, bottomLeft.
        public int value;
    }


    ///MeshFilter meshFilter;
    
    List<Vector3> vertexList = new List<Vector3>();
    List<int> triangleList = new List<int>();

    Square[,] squares;

    public MeshGenerator(Level.Tile[,] map, Vector3 size)
    {
        squares = new Square[map.GetLength(0), map.GetLength(1)];

        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                Square square = new Square();
                float zDepth = 0;
                square.value = map[x, y].value;
                if(square.value == 0)
                {
                    zDepth = size.z;
                }
                
                Square left = GetNeighbour(x, y, 0);
                bool[] vertsSet = new bool[4];
                vertsSet[0] = false;
                vertsSet[1] = false;
                vertsSet[2] = false;
                vertsSet[3] = false;

                if (left != null && left.init && left.value == square.value)
                {
                        square.vertices[0] = left.vertices[1];
                        square.vertices[3] = left.vertices[2];
                        vertsSet[0] = true;
                        vertsSet[3] = true;

                }

                Square top = GetNeighbour(x, y, 1);

                if (top != null && top.init && top.value == square.value)
                {
                    if (!vertsSet[0])
                    {
                        square.vertices[0] = top.vertices[3];
                        vertsSet[0] = true;
                    }

                    square.vertices[1] = top.vertices[2];
                    vertsSet[1] = true;
                }

                Square right = GetNeighbour(x, y, 2);

                if (right != null && right.init && right.value == square.value)
                {
                    if (!vertsSet[1])
                    {
                        square.vertices[1] = right.vertices[0];
                        vertsSet[1] = true;
                    }

                    square.vertices[2] = right.vertices[3];
                    vertsSet[2] = true;
                }

                Square bottom = GetNeighbour(x, y, 2);

                if (bottom != null && bottom.init && bottom.value == square.value)
                {
                    if (!vertsSet[2])
                    {
                        square.vertices[1] = bottom.vertices[1];
                        vertsSet[2] = true;
                    }

                    square.vertices[3] = bottom.vertices[0];
                    vertsSet[3] = true;
                }

                if (!vertsSet[0]) square.vertices[0] = new Vertex(new Vector3(x * size.x, y * size.y + size.y, zDepth));
                if (!vertsSet[1]) square.vertices[1] = new Vertex(new Vector3(x * size.x + size.x, y * size.y + size.y, zDepth)); 
                if (!vertsSet[2]) square.vertices[2] = new Vertex(new Vector3(x * size.x + size.x, y * size.y, zDepth));
                if (!vertsSet[3]) square.vertices[3] = new Vertex(new Vector3(x * size.x, y * size.y, zDepth));
                


                square.init = true;
                squares[x,y] = square;
            }
        }
    }

    /// <summary>
    /// Returns the neighbouring square, or null if not available
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="neighbour">0=left,1= top, 2 = right, 3 = bottom</param>
    /// <returns></returns>
    Square GetNeighbour(int x, int y, int neighbour)//0=left,1= top, 2 = right, 3 = bottom
    {
        Square square = null;
        if (neighbour < 0 || neighbour > 3) return null;

        int neighbourX = 0, neighbourY = 0;
        if(neighbour == 0)//left
        {
            neighbourX = x - 1;
            neighbourY = y;
        }

        if (neighbour == 1)//top
        {
            neighbourX = x;
            neighbourY = y + 1;
        }

        if (neighbour == 2)//right
        {
            neighbourX = x + 1;
            neighbourY = y;
        }

        if (neighbour == 3)//bottom
        {
            neighbourX = x;
            neighbourY = y - 1;
        }

        if (neighbourX >= 0 && neighbourX < squares.GetLength(0) && neighbourY >=  0 && neighbourY < squares.GetLength(1))
        {
            square = squares[x, y];
        }


        return square;
    }


    public Mesh GenerateMesh(int value)
    {
        //generate mesh for squres match value
        vertexList.Clear();
        triangleList.Clear();
        foreach(var square in squares)
        {
            if(square.value == value)
            {
                AddTriangle(square.vertices[0], square.vertices[1], square.vertices[3]);
                AddTriangle(square.vertices[3], square.vertices[1], square.vertices[2]);
            }
        }

        Mesh mesh = new Mesh();

        mesh.vertices = vertexList.ToArray();
        //mesh.uv = newUV;
        mesh.triangles = triangleList.ToArray();
        mesh.RecalculateNormals();

        return mesh;
    }

    void AddTriangle(Vertex A, Vertex B, Vertex C)
    {
        triangleList.Add(AddVertex(A));
        triangleList.Add(AddVertex(B));
        triangleList.Add(AddVertex(C));
    }

    private int AddVertex(Vertex vertex)
    {
        if (vertex.index == -1)
        {
            vertex.index = vertexList.Count;
            vertexList.Add(vertex.position);
        }

        return vertex.index;
    }
}
