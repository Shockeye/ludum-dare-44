﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[ExecuteInEditMode]
public class Level : MonoBehaviour
{

    public class Tile
    {
        //todo enum / but serialization issues?
        public int value = 1; // 0 = background, 1 = foreground, 2 = floor, 3 = ceiling, 4 = wall, 5 = platform,
        public bool mesh = false;

        public Tile[] neighbours = new Tile[4]; //0=left,1= top, 2 = right, 3 = bottom

        public int GetNeighbourValue(int dir)
        {
            
            if(neighbours[dir]==null)
            {
                return 1;
            }
            else
            {
                return neighbours[dir].value;
            }
        }

        public override string ToString()
        {
            string valueString = "Tile. Value = ";
            valueString += value.ToString();
            return valueString;
        }
    }

    public Vector2Int playerSpawn = Vector2Int.zero;

    //this is just a temporary thing
    int tileTypes = 5;
    public int GetNumberOfTileTypes()
    {
        return tileTypes;
    }
    public int width = 100;
    public int height = 50;

    [HideInInspector]
    public Tile[,] map;
    public bool initialised = false;

    public Vector3 gridSize = new Vector3(1f, 1f, 1.5f);
    public int border = 1;

    public Material foreground;
    public Material background;
    public Material wall; // debugging use only, normally will use foreground mat
    public Material floor;
    public Material ceiling;
    public Material platform;


    public string filename;

    List<GameObject> CreatedObjects = new List<GameObject>();

    GameObject backgroundObject;// = new GameObject();
    GameObject foregroundObject;// = new GameObject();
    

    // Start is called before the first frame update
    void Start()
    {
        InitMeshes();
    }

    private void OnDrawGizmosSelected()
    {
        
    }

    public int FlattenArrayIndices(int x, int y)
    {
        return x + y * width;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void InitMeshes()
    {

     
        if (backgroundObject == null)
        {
            backgroundObject = new GameObject();
            backgroundObject.AddComponent<MeshFilter>();
            backgroundObject.AddComponent<MeshRenderer>();
            backgroundObject.transform.parent = this.transform;
            backgroundObject.name = "background";
        }

        if(foregroundObject == null)
        {
            foregroundObject = new GameObject();
            foregroundObject.AddComponent<MeshFilter>();
            foregroundObject.AddComponent<MeshRenderer>();
            foregroundObject.transform.parent = this.transform;
            foregroundObject.name = "foreground";
        }
    }

    public void Display()
    {
        PreprocessMap();
        InitMeshes();
        MeshGenerator generator = new MeshGenerator(map, gridSize);
 

        MeshFilter meshFilter = foregroundObject.GetComponent<MeshFilter>();
        if (meshFilter == null) return;
        MeshRenderer meshRenderer = foregroundObject.GetComponent<MeshRenderer>();
        if (meshRenderer == null) return;
        meshRenderer.material = foreground;

        Mesh mesh = generator.GenerateMesh(1);  //foreground      
        meshFilter.mesh = mesh;


        meshFilter = backgroundObject.GetComponent<MeshFilter>();
        if (meshFilter == null) return;
        meshRenderer = backgroundObject.GetComponent<MeshRenderer>();
        if (meshRenderer == null) return;
        meshRenderer.material = background;

        mesh = generator.GenerateMesh(0);  //background      
        meshFilter.mesh = mesh;


        //walls, floors ceilings, etc :

        foreach (var t in map) t.mesh = false;//reset
        for (int i = 0; i < CreatedObjects.Count; i++)
        {
            GameObject.DestroyImmediate(CreatedObjects[i]); //only in editor change this later
        }


        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                if(map[x,y].mesh == false)
                {
                    
                    map[x, y].mesh = true;
                    //build the mesh // 0 = background, 1 = foreground, 2 = floor, 3 = ceiling, 4 = wall, 5 = platform,
                    if (map[x,y].value == 2) //floor
                    {
                        
                        int i = 1;
                        while(x+i < map.GetLength(0))
                        {
                            if (map[x + i, y].value == map[x, y].value)
                            {
                                map[x + i, y].mesh = true;
                                i++;
                            }
                            else
                            {
                                break;
                            }
                        }

                        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube); //temp for testing
                        obj.layer = 9;
                        obj.GetComponent<MeshRenderer>().material = floor;
                        obj.transform.parent = transform;
                        obj.transform.localScale = new Vector3(gridSize.x * i, gridSize.y, gridSize.z);
                        float xPos = (x * gridSize.x ) +(gridSize.x * i / 2)  ;
                        float yPos = y * gridSize.y + gridSize.y / 2;
                        Vector3 position = new Vector3(xPos, yPos, gridSize.z / 2);
                        obj.transform.position = position;
                        CreatedObjects.Add(obj);

                    }
                    if (map[x, y].value == 3)
                    {

                        int i = 1;
                        while (x + i < map.GetLength(0))
                        {
                            if (map[x + i, y].value == map[x, y].value)
                            {
                                map[x + i, y].mesh = true;
                                i++;
                            }
                            else
                            {
                                break;
                            }
                        }

                        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube); //temp for testing
                        obj.GetComponent<MeshRenderer>().material = ceiling;
                        obj.transform.parent = transform;
                        obj.transform.localScale = new Vector3(gridSize.x * i, gridSize.y, gridSize.z);
                        float xPos = (x * gridSize.x) + (gridSize.x * i / 2);
                        float yPos = y * gridSize.y + gridSize.y / 2;
                        Vector3 position = new Vector3(xPos, yPos, gridSize.z / 2);
                        obj.transform.position = position;
                        CreatedObjects.Add(obj);

                    }

                    if (map[x, y].value == 5)//platform
                    {

                        int i = 1;
                        while (x + i < map.GetLength(0))
                        {
                            if (map[x + i, y].value == map[x, y].value)
                            {
                                map[x + i, y].mesh = true;
                                i++;
                            }
                            else
                            {
                                break;
                            }
                        }

                        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube); //temp for testing
                        obj.GetComponent<MeshRenderer>().material = platform;
                        obj.layer = 9;
                        obj.transform.parent = transform;
                        obj.transform.localScale = new Vector3(gridSize.x * i, gridSize.y, gridSize.z);
                        float xPos = (x * gridSize.x) + (gridSize.x * i / 2);
                        float yPos = y * gridSize.y + gridSize.y / 2;
                        Vector3 position = new Vector3(xPos, yPos, gridSize.z / 2);
                        obj.transform.position = position;
                        CreatedObjects.Add(obj);

                    }

                    if (map[x, y].value == 4)//wall
                    {

                        int i = 1;
                        while (y + i < map.GetLength(0))
                        {
                            if (map[x , y+ i].value == map[x, y].value)
                            {
                                map[x , y+ i].mesh = true;
                                i++;
                            }
                            else
                            {
                                break;
                            }
                        }

                        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube); //temp for testing
                        obj.GetComponent<MeshRenderer>().material = wall;
                        obj.layer = 10;
                        obj.transform.parent = transform;
                        obj.transform.localScale = new Vector3(gridSize.x , gridSize.y * i, gridSize.z);
                        float xPos = x * gridSize.x + gridSize.x / 2;
                        float yPos = (y * gridSize.y) + (gridSize.y * i / 2);
                        Vector3 position = new Vector3(xPos, yPos, gridSize.z / 2);
                        obj.transform.position = position;
                        CreatedObjects.Add(obj);

                    }

                }

            }
        }


    }

    public bool IsWithinBoundary(Vector2Int tile)
    {
        if (tile.x > border && tile.x < width - border && tile.y > border && tile.y < height - border) return true;
        else return false;
    }

    void PreprocessMap()
    {
       
        foreach(var tile in map)
        {
            int configuration = 0;
            if (tile.value > 0)
            {
                if (tile.GetNeighbourValue(0) > 0) configuration = configuration | 1;
                if (tile.GetNeighbourValue(1) > 0) configuration = configuration | 2;
                if (tile.GetNeighbourValue(2) > 0) configuration = configuration | 4;
                if (tile.GetNeighbourValue(3) > 0) configuration = configuration | 8;

                // 0 = background, 1 = foreground, 2 = floor, 3 = ceiling, 4 = wall, 5 = platform,
                
                switch (configuration)
                {
                    case 0: tile.value = 5;  break;//platform
                    case 1: tile.value = 5;  break; //platform
                    case 2: tile.value = 3;  break;//ceiling
                    case 3: tile.value = 3; break;//ceiling
                    case 4: tile.value = 5; break;//platform
                    case 5: tile.value = 5; break; //platform
                    case 6: tile.value = 3; break;//ceiling
                    case 7: tile.value = 3; break;//ceiling
                    case 8: tile.value = 2; break;//floor
                    case 9: tile.value = 2; break;//floor
                    case 10: tile.value = 4; break;//wall
                    case 11: tile.value = 4; break;//wall
                    case 12: tile.value = 2; break;//floor
                    case 13: tile.value = 2; break;//floor
                    case 14: tile.value = 4; break;//wall
                    case 15:
                        {
                            bool corner = false;
                            if(tile.neighbours[0]!= null)//0=left,1= top, 2 = right, 3 = bottom
                            {
                                if (tile.neighbours[0].GetNeighbourValue(1) == 0 || tile.neighbours[0].GetNeighbourValue(3) == 0)
                                {
                                    corner = true;
                                }                                
                            }

                            if (corner == false && tile.neighbours[2] != null)
                            {
                                if (tile.neighbours[2].GetNeighbourValue(1) == 0 || tile.neighbours[2].GetNeighbourValue(3) == 0)
                                {
                                    corner = true;
                                }
                            }

                            if (corner==true)
                            {
                                tile.value = 4;
                            }
                            else tile.value = 1;
                            
                        }
                        break;
                    default: break;
                }
            }


        }
    }

    
    public void InitialiseMap()
    {
        map = new Tile[width, height];
        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                map[x, y] = new Tile();

                if(x > 0)
                {
                    map[x, y].neighbours[0] = map[x-1, y];
                    map[x - 1, y].neighbours[2] = map[x, y];
                }
                else
                {
                    map[x, y].neighbours[2] = null;
                }

                if (y > 0)
                {
                    map[x, y].neighbours[3] = map[x, y-1];
                    map[x, y - 1].neighbours[1] = map[x, y];
                }
                else
                {
                    map[x, y].neighbours[3] = null;
                }
            }
        }

        initialised = true;
        Display();
    }

    public Vector3 GetWorldSpacePositionOfTile(int x, int y)
    {
        return  new Vector3(x, y + gridSize.y / 2, gridSize.z / 2);
    }

    /// <summary>
    /// Returns the Tile at a given set of coordinates on Level map.
    /// NOTE: Will return null if outside Level map.
    /// </summary>
    /// <param name="point">Coordinates on grid</param>
    /// <returns>The Tile at the specifide coordinates or null if none found</returns>
    public Tile GetTileAtGridCoordinates(Vector2Int point)
    {
        if (map == null) return null;

        Tile result = null;

        if (point.x >= 0 && point.x < map.GetLength(0) && point.y >= 0 && point.y < map.GetLength(1))
        {
            result = map[point.x, point.y];
        }

        return result;
    }

    /// <summary>
    /// Converts a world space point on XY plane to a set of grid coordinates.
    /// NOTE: May return a value outside the current Level map.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns>Coordinates of grid square. May be outside of current map</returns>
    public Vector2Int GetGridCoordinatesAtPoint(float x, float y)
    {
        Vector2Int coordinates = new Vector2Int();
        coordinates.x = Mathf.FloorToInt(x / gridSize.x);
        coordinates.y = Mathf.FloorToInt(y / gridSize.y);

        return coordinates;
    }

    public void ResizeMap()
    {
        Tile[,] old = null;
        if (map!= null)
        {
            old = map;
        }

        InitialiseMap();

        if (old != null)
        {
            for (int x = 0; x < old.GetLength(0); x++)
            {
                for (int y = 0; y < old.GetLength(1); y++)
                {
                    if(x<map.GetLength(0) && y < map.GetLength(1))//just in case map has shrunk
                    {
                        map[x, y] = old[x, y];
                    }
                } 
            }
        }

        Display();
    }

    public void SaveLevel(string filename)
    {
        string path = null;
#if UNITY_EDITOR
        path = "Assets/Resources/";
        path += filename;
        path += ".JSON";
        LevelData levelData = new LevelData();
        levelData.width = width;
        levelData.height = height;
        levelData.playerSpawn = playerSpawn;
        levelData.data = new int[width * height];
        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                int i = FlattenArrayIndices(x, y);
                levelData.data[i] = map[x, y].value;
            }
        }

        string str = JsonUtility.ToJson(levelData);
        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }

        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public void LoadLevel(string filename)
    {
        var json = Resources.Load<TextAsset>(filename);


        LevelData levelData = JsonUtility.FromJson<LevelData>(json.text);

        width = levelData.width;
        height = levelData.height;
        playerSpawn = levelData.playerSpawn;

        InitialiseMap();

        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                int i = FlattenArrayIndices(x, y);
                map[x, y].value = levelData.data[i];

            }
        }

        Display();
    }

}
