﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelData 
{

    public int width;
    public int height;
    public Vector2Int playerSpawn;
    public int[] data;
}
